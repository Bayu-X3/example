import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SuitJobs extends StatelessWidget {
  const SuitJobs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 0, 97, 177),
        body: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 450,
                  padding: EdgeInsets.symmetric(vertical: 60, horizontal: 52),
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Color(0xFFCDE9FF),
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      color: Colors.blue,
                      width: 5,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // Image
                          Image.asset(
                            '../../assets/SuitJobs.png', // replace with actual image path
                            width: 49,
                            height: 49,
                          ),
                          SizedBox(width: 10), // Add spacing
                          // SuitJobs Text
                          Text(
                            "SuitJobs",
                            style: GoogleFonts.inter(
                              fontSize: 32,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10), // Add spacing
                      Text(
                        "Login Account",
                        style: GoogleFonts.inter(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(height: 15), // Add spacing
                      Text(
                        "Login Account to enjoy all the services without any ads for free!",
                        style: GoogleFonts.inter(
                          fontSize: 15,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center, // Center the text
                      ),
                      SizedBox(height: 15), // Add spacing
                      // Username Input
                      Container(
                        width: 234,
                        height: 48,
                        margin: EdgeInsets.symmetric(vertical: 8),
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: 'Username',
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                      // Password Input
                      Container(
                        width: 234,
                        height: 48,
                        margin: EdgeInsets.symmetric(vertical: 8),
                        child: TextField(
                          obscureText: true,
                          decoration: InputDecoration(
                            hintText: 'Password',
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20), // Add spacing
                      // Login Button
                      Container(
                        width: 234,
                        height: 48,
                        margin: EdgeInsets.symmetric(vertical: 8),
                        child: ElevatedButton(
                          onPressed: () {
                            // Add login functionality here
                          },
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFF84C7AE),
                          ),
                          child: Text(
                            'Login',
                            style: GoogleFonts.inter(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10), // Add spacing
                      // "Belum memiliki akun? Daftar di sini" Text
                      Text(
                        'Belum memiliki akun? ',
                        style: GoogleFonts.inter(
                          fontSize: 15,
                          color: Colors.black,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          // Add registration navigation or functionality here
                        },
                        child: Text(
                          'Daftar di sini',
                          style: GoogleFonts.inter(
                            fontSize: 15,
                            color: Colors.blue, // Different color for this text
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
